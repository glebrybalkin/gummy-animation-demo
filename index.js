const manGummyImagesAnimationParams = {
    imagesSize: {width: 21, height: 28},
    imagesPositions: [
        {top: 101, left: 120, rotation: -14.31},
        {top: 128, left: 128, rotation: -14.31},
        {top: 154, left: 136, rotation: -14.31},
        {top: 89, left: 137, rotation: -62.31},
        {top: 171, left: 158, rotation: -50.31},
        {top: 100, left: 161, rotation: -62.31},
        {top: 186, left: 179, rotation: -50.31},
        {top: 81, left: 104, rotation: -88.31},
        {top: 69, left: 128, rotation: -132.31},
        {top: 44, left: 137, rotation: -176.31},
        {top: 19, left: 127, rotation: 137.69},
        {top: 9, left: 101, rotation: 89.69},
        {top: 20, left: 76, rotation: 37.69},
        {top: 46, left: 65, rotation: -1.31},
        {top: 71, left: 79, rotation: -50.31},
        {top: 102, left: 100, rotation: 33.69},
        {top: 180, left: 130, rotation: 21.69},
        {top: 123, left: 85, rotation: 33.69},
        {top: 204, left: 119, rotation: 21.69}
    ]
}

const liverGummyImagesAnimationParams = {
    imagesSize: {width: 21, height: 28},
    imagesPositions: [
        {top: 105, left: 132, rotation: 177},
        {top: 130, left: 128, rotation: -157},
        {top: 51, left: 63, rotation: 82},
        {top: 58, left: 38, rotation: 63},
        {top: 73, left: 16, rotation: 38},
        {top: 96, left: 5, rotation: 12},
        {top: 122, left: 1, rotation: -0.32},
        {top: 148, left: 3, rotation: -7.6},
        {top: 174, left: 9, rotation: -20.1},
        {top: 196, left: 23, rotation: -40.5},
        {top: 193, left: 44, rotation: -141.6},
        {top: 175, left: 63, rotation: -122.5},
        {top: 165, left: 88, rotation: -104.5},
        {top: 152, left: 111, rotation: -130.5},
        {top: 158, left: 133, rotation: -84.6},
        {top: 154, left: 159, rotation: -112.6},
        {top: 138, left: 181, rotation: -134.5},
        {top: 118, left: 198, rotation: -141},
        {top: 96, left: 212, rotation: -151.3},
        {top: 71, left: 215, rotation: 154.5},
        {top: 57, left: 195, rotation: 95.8},
        {top: 54, left: 168, rotation: 95.8},
        {top: 52, left: 143, rotation: 95.8},
        {top: 50, left: 117, rotation: 90},
        {top: 50, left: 89, rotation: 90}
    ]
}

const lampGummyImagesAnimationParams = {
    imagesSize: {width: 21, height: 28},
    imagesPositions: [
        {top: 180, left: 87, rotation: 156},
        {top: 154, left: 77, rotation: 156},
        {top: 131, left: 59, rotation: 127},
        {top: 110, left: 40, rotation: 144},
        {top: 84, left: 29, rotation: 164},
        {top: 55, left: 31, rotation: -160},
        {top: 154, left: 128, rotation: -104},
        {top: 161, left: 101, rotation: -104},
        {top: 204, left: 102, rotation: 127},
        {top: 213, left: 129, rotation: 79},
        {top: 195, left: 148, rotation: 31},
        {top: 166, left: 151, rotation: -15},
        {top: 140, left: 144, rotation: -15},
        {top: 112, left: 144, rotation: 18.5},
        {top: 84, left: 149, rotation: 7},
        {top: 56, left: 145, rotation: -18},
        {top: 31, left: 130, rotation: -38},
        {top: 16, left: 105, rotation: -75},
        {top: 30, left: 48, rotation: -141},
        {top: 16, left: 74, rotation: -101}
    ]
}

const urinaryGummyImagesAnimationParams = {
    imagesSize: {width: 21, height: 28},
    imagesPositions: [
        {top: 50, left: 71, rotation: -57},
        {top: 76, left: 81, rotation: 15},
        {top: 200, left: 144, rotation: -13},
        {top: 205, left: 120, rotation: -13},
        {top: 172, left: 138, rotation: -13},
        {top: 177, left: 114, rotation: -13},
        {top: 145, left: 131, rotation: -13},
        {top: 150, left: 107, rotation: -13},
        {top: 118, left: 151, rotation: -45},
        {top: 125, left: 180, rotation: -99},
        {top: 107, left: 202, rotation: -155},
        {top: 78, left: 204, rotation: 175},
        {top: 49, left: 199, rotation: 166},
        {top: 28, left: 179, rotation: 114},
        {top: 22, left: 149, rotation: 83},
        {top: 38, left: 123, rotation: 26},
        {top: 67, left: 127, rotation: -39},
        {top: 91, left: 140, rotation: -1},
        {top: 117, left: 125, rotation: -13},
        {top: 123, left: 100, rotation: -13},
        {top: 105, left: 78, rotation: -21},
        {top: 135, left: 80, rotation: 18},
        {top: 154, left: 57, rotation: 74},
        {top: 145, left: 29, rotation: 131},
        {top: 121, left: 15, rotation: 158},
        {top: 91, left: 7, rotation: 175},
        {top: 62, left: 18, rotation: -136},
        {top: 45, left: 42, rotation: -107}
    ]
}

const leaveGummyImagesAnimationParams = {
    imagesSize: {width: 21, height: 28},
    imagesPositions: [
        {top: 153, left: 22, rotation: -92},
        {top: 118, left: 193, rotation: -114},
        {top: 151, left: 49, rotation: -92},
        {top: 128, left: 169, rotation: -114},
        {top: 172, left: 11, rotation: -37},
        {top: 129, left: 212, rotation: -169},
        {top: 189, left: 31, rotation: -59},
        {top: 154, left: 201, rotation: -146},
        {top: 199, left: 57, rotation: -77},
        {top: 174, left: 182, rotation: -128.5},
        {top: 204, left: 85, rotation: -84},
        {top: 190, left: 159, rotation: -122},
        {top: 191, left: 132, rotation: -161},
        {top: 197, left: 108, rotation: -43},
        {top: 163, left: 142, rotation: -169},
        {top: 175, left: 88, rotation: -34},
        {top: 135, left: 146, rotation: -174.5},
        {top: 151, left: 73, rotation: -29},
        {top: 107, left: 147, rotation: 173},
        {top: 125, left: 62, rotation: -16},
        {top: 80, left: 137, rotation: 149},
        {top: 96, left: 60, rotation: 8},
        {top: 56, left: 120, rotation: 140},
        {top: 68, left: 67, rotation: 17},
        {top: 36, left: 101, rotation: 137},
        {top: 42, left: 76, rotation: 19.5}
    ]
}

const heartGummyImagesAnimationParams = {
    imagesSize: {width: 36, height: 48},
    imagesPositions: [
        {top: 34, left: 155, rotation: 47},
        {top: 11, left: 197, rotation: 81},
        {top: 190, left: 176, rotation: -133},
        {top: 156, left: 211, rotation: -136},
        {top: 120, left: 241, rotation: -147},
        {top: 31, left: 240, rotation: 141},
        {top: 75, left: 256, rotation: 173},
        {top: 39, left: 107, rotation: 119},
        {top: 27, left: 60, rotation: 85},
        {top: 194, left: 125, rotation: -60},
        {top: 171, left: 82, rotation: -58},
        {top: 144, left: 44, rotation: -47},
        {top: 57, left: 24, rotation: 25},
        {top: 104, left: 19, rotation: -7}
    ]
}

const moonGummyImagesAnimationParams = {
    imagesSize: {width: 36, height: 48},
    imagesPositions: [
        {top: 128, left: 202, rotation: 37},
        {top: 163, left: 165, rotation: 57},
        {top: 180, left: 120, rotation: 78},
        {top: 177, left: 72, rotation: 110},
        {top: 149, left: 32, rotation: 140},
        {top: 106, left: 12, rotation: 167},
        {top: 57, left: 15, rotation: -155},
        {top: 93, left: 194, rotation: 75},
        {top: 100, left: 145, rotation: 90},
        {top: 84, left: 99, rotation: 125},
        {top: 44, left: 69, rotation: 159},
        {top: 17, left: 43, rotation: -132}
    ]
}

const stomachGummyImagesAnimationParams = {
    imagesSize: {width: 21, height: 28},
    imagesPositions: [
        {top: 89, left: 204, rotation: 165},
        {top: 69, left: 185, rotation: 116},
        {top: 58, left: 160, rotation: 109},
        {top: 42, left: 139, rotation: 160},
        {top: 24, left: 127, rotation: 83},
        {top: 36, left: 106, rotation: 18},
        {top: 62, left: 107, rotation: -15},
        {top: 86, left: 116, rotation: -15},
        {top: 113, left: 120, rotation: 1},
        {top: 135, left: 104, rotation: 73},
        {top: 138, left: 77, rotation: 87},
        {top: 138, left: 47, rotation: 87},
        {top: 153, left: 25, rotation: 30},
        {top: 116, left: 207, rotation: -178},
        {top: 143, left: 203, rotation: -160},
        {top: 168, left: 188, rotation: -135},
        {top: 186, left: 166, rotation: -109},
        {top: 194, left: 139, rotation: -97},
        {top: 193, left: 111, rotation: -73},
        {top: 182, left: 85, rotation: -60},
        {top: 172, left: 60, rotation: -108},
        {top: 196, left: 49, rotation: -160},
        {top: 205, left: 27, rotation: -83},
        {top: 183, left: 18, rotation: 0}
    ]
}

const bonesGummyImagesAnimationParams = {
    imagesSize: {width: 21, height: 28},
    imagesPositions: [
        {top: 232, left: 55, rotation: -131},
        {top: 38, left: 168, rotation: 49},
        {top: 210, left: 77, rotation: -131},
        {top: 49, left: 140, rotation: 86},
        {top: 195, left: 103, rotation: -95},
        {top: 53, left: 110, rotation: 77},
        {top: 191, left: 132, rotation: -94},
        {top: 74, left: 91, rotation: 7},
        {top: 171, left: 140, rotation: 109},
        {top: 96, left: 108, rotation: -62},
        {top: 147, left: 119, rotation: 158},
        {top: 110, left: 133, rotation: -56},
        {top: 126, left: 99, rotation: 107},
        {top: 134, left: 149, rotation: -19},
        {top: 153, left: 166, rotation: -81},
        {top: 104, left: 78, rotation: 159},
        {top: 138, left: 190, rotation: -159},
        {top: 109, left: 54, rotation: 21},
        {top: 110, left: 198, rotation: -172},
        {top: 138, left: 47, rotation: 8},
        {top: 85, left: 215, rotation: -134},
        {top: 163, left: 30, rotation: 46},
        {top: 16, left: 191, rotation: -134},
        {top: 64, left: 236, rotation: -134},
        {top: 184, left: 9, rotation: 46}
    ]
}

const lungsGummyImagesAnimationParams = {
    imagesSize: {width: 21, height: 28},
    imagesPositions: [
        {top: 100, left: 108, rotation: 45},
        {top: 100, left: 140, rotation: 135},
        {top: 88, left: 160, rotation: -180},
        {top: 152, left: 160, rotation: -180},
        {top: 63, left: 171, rotation: 99},
        {top: 72, left: 201, rotation: 120},
        {top: 94, left: 222, rotation: 153},
        {top: 185, left: 209, rotation: 108},
        {top: 184, left: 235, rotation: -174},
        {top: 153, left: 237, rotation: -180},
        {top: 122, left: 234, rotation: 165},
        {top: 120, left: 160, rotation: -180},
        {top: 175, left: 179, rotation: 108},
        {top: 74, left: 124, rotation: 0},
        {top: 120, left: 87, rotation: 0},
        {top: 42, left: 124, rotation: 0},
        {top: 88, left: 87, rotation: 0},
        {top: 173, left: 66, rotation: 69},
        {top: 152, left: 87, rotation: 0},
        {top: 63, left: 76, rotation: 81},
        {top: 72, left: 46, rotation: 60},
        {top: 94, left: 25, rotation: 27},
        {top: 185, left: 38, rotation: 72},
        {top: 184, left: 12, rotation: -5.5},
        {top: 153, left: 10, rotation: 0},
        {top: 122, left: 13, rotation: 15}
    ]
}

const mirrorGummyImagesAnimationParams = {
    imagesSize: {width: 21, height: 28},
    imagesPositions: [
        {top: 192, left: 138, rotation: 3.35},
        {top: 214, left: 127, rotation: -104},
        {top: 200, left: 107, rotation: -31},
        {top: 165, left: 132, rotation: -18},
        {top: 176, left: 97, rotation: -18},
        {top: 140, left: 123, rotation: -18},
        {top: 149, left: 88, rotation: -18},
        {top: 129, left: 71, rotation: -64},
        {top: 115, left: 48, rotation: -51},
        {top: 94, left: 32, rotation: -33},
        {top: 69, left: 23, rotation: -10},
        {top: 42, left: 26, rotation: 16},
        {top: 118, left: 128, rotation: -150},
        {top: 95, left: 139, rotation: -155},
        {top: 70, left: 145, rotation: -177},
        {top: 45, left: 141, rotation: 169},
        {top: 22, left: 130, rotation: 149},
        {top: 5, left: 111, rotation: 110},
        {top: -2.4, left: 84, rotation: 95},
        {top: 2, left: 58, rotation: 61},
        {top: 19, left: 38, rotation: 34},
        {top: 106, left: 95, rotation: -106},
        {top: 90, left: 115, rotation: -154},
        {top: 64, left: 117, rotation: 170},
        {top: 40, left: 108, rotation: 147},
        {top: 25, left: 87, rotation: 99},
        {top: 31, left: 62, rotation: 48},
        {top: 55, left: 52, rotation: 7},
        {top: 80, left: 55, rotation: -14},
        {top: 101, left: 70, rotation: -55}
    ]
}

const smileGummyImagesAnimationParams = {
    imagesSize: {width: 42, height: 56},
    imagesPositions: [
        {top: 126, left: 153, rotation: -132.8},
        {top: 156, left: 104, rotation: -104.9},
        {top: 157, left: 47, rotation: -74.4},
        {top: 56, left: 110, rotation: -5.5},
        {top: 75, left: 45, rotation: -30.9}
    ]
}

const smileCapsuleImagesAnimationParams = {
    imagesSize: {width: 44, height: 78},
    imagesPositions: [
        {top: 119, left: 170, rotation: -130},
        {top: 154, left: 108, rotation: -107.97},
        {top: 164, left: 38, rotation: -83},
        {top: 54, left: 106, rotation: -11.86},
        {top: 75, left: 43, rotation: -30}
    ]
}

class ImagesAnimation {
    constructor(containerId, params, imgUrl) {
        console.log(params)
        this.container = document.querySelector('#' + containerId)
        this.params = params
        this.imgUrl = imgUrl
        this.generateImages()
    }

    generateImages() {
        for (let i = 0; i < this.params.imagesPositions.length; i++) {
            const img = document.createElement('img')
            img.src = this.imgUrl
            img.alt = ''
            img.width = this.params.imagesSize.width
            img.height = this.params.imagesSize.height
            img.style.position = 'absolute'
            img.style.top = '150px'
            img.style.left = 610 + i * this.params.imagesSize.width + 'px'
            this.container.appendChild(img)
        }
    }

    animateImages() {
        const gummyImages = this.container.childNodes
        for (let i = 0; i < gummyImages.length; i++) {
            const currentImgParams = this.params.imagesPositions[i]
            gsap.fromTo(gummyImages[i],
                {top: 120, left: 610 + i * this.params.imagesSize.width, rotation: 0},
                {
                    duration: 1.0,
                    ease: 'none',
                    delay: 0.5,
                    top: 120,
                    left: 200 + i * this.params.imagesSize.width,
                    rotation: 0
                }
            ).then(() => {
                gsap.to(gummyImages[i],
                    {
                        duration: 1.0,
                        top: currentImgParams.top,
                        left: currentImgParams.left,
                        rotation: currentImgParams.rotation
                    }
                )
            })
        }
    }
}

// to create animation initialize class "ImagesAnimation" with three params (id of container where animation will be place; params of animation; url to image that will be used to animation )


// Example of usage
let gummyGreenManAnimation = new ImagesAnimation('gummyGreenMan', lungsGummyImagesAnimationParams, 'images/gummy_pink.png')
gummyGreenManAnimation.animateImages()

